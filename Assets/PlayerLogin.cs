﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerLogin : NetworkManager
{
    Vector2 spawnPos = new Vector2(-8, -3);

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        InitChat();
        GameObject player;
        player = GameObject.Instantiate((GameObject)Resources.Load("Runner1"), spawnPos, Quaternion.identity);
        player.GetComponent<Movement>().pnum = numPlayers;
        NetworkServer.AddPlayerForConnection(conn, player);
        if(numPlayers == 2)
        {
            GameObject.Find("Canvas").GetComponent<GameStatus>().readyForCountdown = true;
            GameObject.Find("Canvas").GetComponent<GameStatus>().chatAvaliable = true;
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
    }

    public void InitChat()
    {

        for (int i = 0; i < 6; i++)
        {
            GameObject.Find("Canvas").GetComponent<GameStatus>().chat.Add("");
            GameObject.Find("Canvas").GetComponent<GameStatus>().nMessages = GameObject.Find("Canvas").GetComponent<GameStatus>().chat.Count;
        }
    }
}
