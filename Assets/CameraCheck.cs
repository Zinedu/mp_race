﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class CameraCheck : NetworkBehaviour
{
    GameObject cam;

    // Update is called once per frame
    private void Start()
    {
        cam = GameObject.Find("Main Camera");
    }
    void FixedUpdate()
    {

        if (isLocalPlayer)
        {
            cam.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, -10);
        }
    }
}
