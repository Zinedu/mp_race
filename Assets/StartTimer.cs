﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class StartTimer : NetworkBehaviour
{
    // Start is called before the first frame update

    // Update is called once per frame
    void FixedUpdate()
    {
        if(GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown > 0 && GameObject.Find("Canvas").GetComponent<GameStatus>().readyForCountdown)
        {
            if (isServer) { 
                
                StartCoroutine(SecondDelay());
            }
        }
    }
[Server]
    public IEnumerator SecondDelay()
    {
        yield return new WaitForSeconds(1f);
        if(GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown > 0) {
            GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown--;
        }
        
    }
}
