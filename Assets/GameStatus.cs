﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class GameStatus : NetworkBehaviour
{
    [SyncVar]
    public int currentCountdown = 3;
    [SyncVar]
    public string winner = "no";
    [SyncVar]
    public bool readyForCountdown = false;
    [SyncVar]
    public bool raceOver = false;  
    public SyncList<string> chat = new SyncList<string>();
    [SyncVar]
    public int nMessages = 0;
    [SyncVar]
    public bool chatAvaliable = false;


}
