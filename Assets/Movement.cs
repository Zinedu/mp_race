﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class Movement : NetworkBehaviour
{
    // Start is called before the first frame update
    bool onGround = false;
    int speed = 4;
    public int laps = 0;
    public int pnum;
    public bool checkpointed = false;

    
    // Update is called once per frame
    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            if (GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown == 0 && !GameObject.Find("Canvas").GetComponent<GameStatus>().raceOver)
            {
                if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow)) {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    if (onGround)
                    {
                        this.gameObject.GetComponent<Animator>().SetBool("Running", true);
                        this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + speed * Time.deltaTime, this.gameObject.transform.position.y);
                        
                    }
                    else
                    {
                        this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + speed * Time.deltaTime, this.gameObject.transform.position.y);
                    }
                        if (this.gameObject.GetComponent<SpriteRenderer>().flipX)
                        {
                            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
                        }
                }
                    if (Input.GetKey(KeyCode.LeftArrow))
                    {
                        if (onGround)
                        {
                            this.gameObject.GetComponent<Animator>().SetBool("Running", true);
                            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + -speed * Time.deltaTime, this.gameObject.transform.position.y);

                        }
                        else
                        {
                            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + -speed * Time.deltaTime, this.gameObject.transform.position.y);
                        }
                        if (!this.gameObject.GetComponent<SpriteRenderer>().flipX)
                        {
                            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
                        }
                    }
                }
                else
                {
                    this.gameObject.GetComponent<Animator>().SetBool("Running", false);
                }
                if (Input.GetKey(KeyCode.UpArrow) && onGround)
                {
                    onGround = false;
                    this.gameObject.GetComponent<Animator>().SetTrigger("Jump");
                    this.gameObject.GetComponent<NetworkAnimator>().SetTrigger("Jump");
                    this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 400));
                }
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    SendMessage();
                }
            }
            this.gameObject.transform.Find("PlayerCanvas/LapsText").GetComponent<Text>().text = "Laps: " + laps + "/3";
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if((collision.gameObject.name == "Ground" || collision.gameObject.name == "Platforms") && this.gameObject.GetComponent<Rigidbody2D>().velocity.y < 5)
        {
            this.gameObject.GetComponent<Animator>().SetTrigger("Landed");
            this.gameObject.GetComponent<NetworkAnimator>().SetTrigger("Landed");
            onGround = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(collision.gameObject.name == "CheckPoint")
        {
            this.checkpointed = true;
        }
        
        if(collision.gameObject.name == "Goal" && checkpointed)
        {
            if(laps < 2)
            {
                laps++;
            }
            else if(this.laps == 3 && GameObject.Find("Canvas").GetComponent<GameStatus>().winner == "no")
            {
                switch (pnum)
                {
                    case 0:
                        GameObject.Find("Canvas").GetComponent<GameStatus>().winner = "P1";
                        GameObject.Find("Canvas").GetComponent<GameStatus>().raceOver = true;
                        break;
                    case 1:
                        GameObject.Find("Canvas").GetComponent<GameStatus>().winner = "P2";
                        GameObject.Find("Canvas").GetComponent<GameStatus>().raceOver = true;
                        break;
                }
            }
            this.checkpointed = false;
        }
    }

    public void SendMessage()
    {
        if (!isServer) {
            if (this.transform.Find("PlayerCanvas/Chat/ChatInput").GetComponent<InputField>().text != null)
            {
                MessageCommand(this.transform.Find("PlayerCanvas/Chat/ChatInput").GetComponent<InputField>().text);
            }
        }
        else if(isServer) {
            if (this.transform.Find("PlayerCanvas/Chat/ChatInput").GetComponent<InputField>().text != null)
            {
                HostMessageCommand(this.transform.Find("PlayerCanvas/Chat/ChatInput").GetComponent<InputField>().text);
            }         
        }
               
 //       this.transform.Find("PlayerCanvas/Chat/ChatInput").GetComponent<InputField>().text = "";
    }
    [Command]
    public void MessageCommand(string a)
    {
        GameObject.Find("Canvas").GetComponent<GameStatus>().chat.Add("P" + this.pnum + ": " + a);
        GameObject.Find("Canvas").GetComponent<GameStatus>().nMessages++;
    }

    public void HostMessageCommand(string a)
    {
        GameObject.Find("Canvas").GetComponent<GameStatus>().chat.Add("P" + this.pnum + ": " + a);
        GameObject.Find("Canvas").GetComponent<GameStatus>().nMessages++;
    }

}
