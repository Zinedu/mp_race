﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class ChatHandler : MonoBehaviour
{


    void FixedUpdate()
    {
        if (GameObject.Find("Canvas").GetComponent<GameStatus>().chatAvaliable)
        {
            this.gameObject.GetComponent<Text>().text = GameObject.Find("Canvas").GetComponent<GameStatus>().chat[GameObject.Find("Canvas").GetComponent<GameStatus>().nMessages - 3] + "\n" + GameObject.Find("Canvas").GetComponent<GameStatus>().chat[GameObject.Find("Canvas").GetComponent<GameStatus>().nMessages - 2] + "\n" + GameObject.Find("Canvas").GetComponent<GameStatus>().chat[GameObject.Find("Canvas").GetComponent<GameStatus>().nMessages - 1];
        }
        else
        {
            this.gameObject.GetComponent<Text>().text = "Preparing chat";
        }
    }

}
