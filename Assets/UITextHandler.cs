﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class UITextHandler : NetworkBehaviour
{
    // Update is called once per frame
    void FixedUpdate()
    {
        if(GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown > 0)
        {
            this.gameObject.GetComponent<Text>().text = GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown.ToString();
        }
        else if (GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown == 0 && GameObject.Find("Canvas").GetComponent<GameStatus>().winner == "no") 
        {

            this.gameObject.GetComponent<Text>().text = " ";
        }
        else if(GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown == 0 && GameObject.Find("Canvas").GetComponent<GameStatus>().winner == "P1")
        {
            this.gameObject.GetComponent<Text>().text = "P1 Winner";
        }
        else if(GameObject.Find("Canvas").GetComponent<GameStatus>().currentCountdown == 0 && GameObject.Find("Canvas").GetComponent<GameStatus>().winner == "P2")
        {
            this.gameObject.GetComponent<Text>().text = "P2 Winner";
        }
    }
}
